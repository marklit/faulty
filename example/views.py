import mimetypes
from random import choice, randint
from textwrap import wrap

from django.http import HttpResponse

from .tasks import log_response


def endpoint(request):
    if randint(0, 3) == 3: # 25% chance of raising an error
        raise choice((AssertionError, IndexError, KeyError))

    # Pick a random content type:
    content_type = choice(mimetypes.types_map.values())
    content_length = randint(512, 2048)

    # Create a random string between 512 and 2048 characters long
    # and line wrap it every 50 characters
    choices = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    content = ''.join([choice(choices)
                       for _ in range(content_length)])
    content = '\n'.join(wrap(content, width=50))

    # Send the content, it's length and type to this task
    log_response.delay(content, content_length, content_type)

    return HttpResponse(content, content_type=content_type)
