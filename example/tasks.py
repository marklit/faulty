import hashlib
import logging
from random import choice

from celery import task
from celery.utils.log import get_task_logger


logger = get_task_logger(__name__)


@task()
def log_response(content, content_length, content_type):
    log_method = choice((logging.info,
                         logging.warning,
                         logging.error,
                         logging.critical))

    hasher = hashlib.sha1()
    hasher.update(content)
    log_method('{0} {1} {2}'.format(hasher.hexdigest(),
                                    content_length,
                                    content_type))
    return True
